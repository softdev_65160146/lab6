/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.lab6;

/**
 *
 * @author admin
 */
public class OXOOP {
    public static void main(String[] args) {
        Game game = new Game();
        game.showWelcome();
        game.newBoard();

        while (true) {
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if(game.isFinish()){
                game.showTable();
                game.showResult();
                game.showStat();
                if(game.doContinue()){
                    game.newBoard();
                } else{
                    break;
                }
            }
        }

    }
}
